lint:
	flake8 -v --ignore=D,D100,D104,D107,C400,C401,C402,C407 --statistics pycron

test:
	pytest -vvv --cov-report term-missing --cov=pycron tests/

doc:
	sphinx-build -b html docs/source/ docs/build
