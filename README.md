# Badges

[![pipeline status](https://gitlab.com/Red-Sky/pycron/badges/master/pipeline.svg)](https://gitlab.com/Red-Sky/pycron/-/commits/master)
[![coverage report](https://gitlab.com/Red-Sky/pycron/badges/master/coverage.svg)](https://gitlab.com/Red-Sky/pycron/-/commits/master)
[![python versions](https://img.shields.io/badge/python-3.6%20%7C%203.7%20%7C%203.8-informational)](https://gitlab.com/Red-Sky/pycron/-/commits/master)
[![license](https://img.shields.io/pypi/l/pycron)](https://choosealicense.com/licenses/mit/)