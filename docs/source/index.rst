.. PyCron documentation master file, created by
   sphinx-quickstart on Fri Jan 31 18:09:34 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyCron's documentation!
==================================

.. seealso::
    :doc:`api_doc/index` for more information on both API and their intended purpose.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
