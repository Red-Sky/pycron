# Cron fields ranges
MINUTE_RANGE = (0, 59)
HOUR_RANGE = (0, 23)
DAYOFMONTH_RANGE = (1, 31)
MONTH_RANGE = (1, 12)
DAYOFWEEK_RANGE = (0, 6)

# Number of days for each month
# TODO: leap year
DAYS_IN_MONTH = (31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

# Fields extra allowed values
MONTH_EXTRA_VALUES = ('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC')
DAY_OF_WEEK_EXTRA_VALUES = ('SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT')

# Cron separators
ASTERISK_OPERATOR = '*'
COMMA_OPERATOR = ','
HYPHEN_OPERATOR = '-'
SLASH_OPERATOR = '/'
