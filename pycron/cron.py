# Local libraries
import pycron.field as fields


class Cron:
    """A cron has five fields for specifying day, date and time followed by the command to be run at that interval.

    The value column can have a * or a list of elements separated by commas. An element is either a number in the ranges
    shown above or two numbers in the range separated by a hyphen (meaning an inclusive range)

    Notes:
        * Repeat pattern like /2 for every 2 minutes or /10 for every 10 minutes is not supported by all operating
          systems. If you try to use it and crontab complains it is probably not supported.

        * The specification of days can be made in two fields: month day and weekday. If both are specified in an entry,
          they are cumulative meaning both of the entries will get executed .

    """

    def __init__(self, minutes='*', hour='*', day_of_month='*', month='*', day_of_week='*'):
        # Set fields
        self._minutes = fields.Minutes(minutes)
        self._hour = fields.Hour(hour)
        self._day_of_month = fields.DayOfMonth(day_of_month)
        self._month = fields.Month(month)
        self._day_of_week = fields.DayOfWeek(day_of_week)

    def __repr__(self):
        return 'Cron ({}, {}, {}, {}, {})'.format(
            self.minutes, self.hour, self.day_of_month, self.month, self.day_of_week
        )

    @property
    def minutes(self):
        """Returns the minutes field (1st) of a cron."""
        return self._minutes

    @minutes.setter
    def minutes(self, minutes):
        """Updates the minutes field (1st) of a cron."""
        self._minutes = fields.Minutes(minutes)

    @property
    def hour(self):
        """Returns the hour field (2nd) of a cron."""
        return self._hour

    @hour.setter
    def hour(self, hour):
        """Updates the hour field (2nd) of a cron."""
        self._hour = fields.Hour(hour)

    @property
    def day_of_month(self):
        """Returns the day of month field (3rd) of a cron."""
        return self._day_of_month

    @day_of_month.setter
    def day_of_month(self, day_of_month):
        """Updates the day of month field (3rd) of a cron."""
        self._day_of_month = fields.DayOfMonth(day_of_month)

    @property
    def month(self):
        """Returns the month field (4th) of a cron."""
        return self._month

    @month.setter
    def month(self, month):
        """Updates the month field (4th) of a cron."""
        self._month = fields.Month(month)

    @property
    def day_of_week(self):
        """Returns the minutes field (5th) of a cron."""
        return self._day_of_week

    @day_of_week.setter
    def day_of_week(self, day_of_week):
        """Updates the day of week field (5th) of a cron."""
        self._day_of_week = fields.DayOfWeek(day_of_week)
