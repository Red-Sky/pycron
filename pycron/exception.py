

class PyCronException(Exception):
    """Base class for all project exceptions."""


class ValidationException(PyCronException):
    """Exception class used when field and value validation failed (invalid range or characters)."""


class NextDateException(PyCronException):
    """Exception class used when the generation of a next date failed."""
