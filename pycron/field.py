# Standard libraries
import enum

# Local libraries
import pycron.constants as constants
import pycron.value as values


class FieldType(enum.Enum):
    """Available types for cron fields."""
    MINUTES = enum.auto()
    HOUR = enum.auto()
    DAY_OF_MONTH = enum.auto()
    MONTH = enum.auto()
    DAY_OF_WEEK = enum.auto()


class Field(object):
    """Fields are used to represent day, date and time of the command to be run at that interval.

    Five fields are universally used:
        * Minutes (between 0 and 59)
        * Hour (between 0 and 23)
        * Day of the month (between 1 and 31)
        * Month (between 1 and 12 or 'JAN' and 'DEC')
        * Day of the week (between 0=Sunday and 6=Saturday).

    """

    def __init__(self, value, min=None, max=None, extra_values=None, type=FieldType.MINUTES):
        # Cast to proper value
        _value = values.ValueFactory.create(value)
        # Validate value with eventually boundaries or extra values
        _value.validate(min=min, max=max, extra_values=extra_values)
        # Everything is fine
        self._value = _value
        self._min = min
        self._max = max
        # Sanity checks on type
        if type not in set(item for item in FieldType):
            raise ValueError('The type of a field must be a valid type defined in FieldType enum.')
        self._type = type

    def __repr__(self):
        return f'{self.__class__.__name__} ({self.value})'

    @property
    def value(self):
        return self._value

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    @property
    def type(self):
        return self._type


class Minutes(Field):

    def __init__(self, value):
        # Init parent
        super().__init__(value=value, min=0, max=59, type=FieldType.MINUTES)


class Hour(Field):

    def __init__(self, value):
        # Init parent
        super().__init__(value=value, min=0, max=23, type=FieldType.HOUR)


class DayOfMonth(Field):

    def __init__(self, value):
        # Init parent
        super().__init__(value=value, min=1, max=31, type=FieldType.DAY_OF_MONTH)


class Month(Field):

    def __init__(self, value):
        # Init parent
        super().__init__(value=value, min=1, max=12, extra_values=constants.MONTH_EXTRA_VALUES, type=FieldType.MONTH)


class DayOfWeek(Field):

    def __init__(self, value):
        # Init parent
        super().__init__(value=value, min=0, max=6, extra_values=constants.DAY_OF_WEEK_EXTRA_VALUES,
                         type=FieldType.DAY_OF_WEEK)
