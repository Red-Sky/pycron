# Local libraries
import pycron.constants as constants
import pycron.field as fields
import pycron.value as values


def generate_values(field):
    """Generate all possible values for the given field."""
    if not isinstance(field, fields.Field):
        raise ValueError('Can only generate values from a Field instance.')
    if field.value.type == values.ValueType.ALWAYS:
        for value in range(field.min, field.max + 1):
            yield value
    elif field.value.type == values.ValueType.UNITY:
        if isinstance(field.value.value, int):
            yield field.value.value
        else:
            if field.type == fields.FieldType.MONTH:
                yield constants.MONTH_EXTRA_VALUES.index(field.value.value.upper()) + 1
            if field.type == fields.FieldType.DAY_OF_WEEK:
                yield constants.DAY_OF_WEEK_EXTRA_VALUES.index(field.value.value.upper())
    elif field.value.type == values.ValueType.RANGE:
        for value in range(field.value.min, field.value.max + 1):
            yield value
    elif field.value.type == values.ValueType.STEP:
        # ALWAYS or RANGE possible
        if field.value.value.type == values.ValueType.ALWAYS:
            start, stop = field.min, field.max
        else:
            start, stop = field.value.value.min, field.value.value.max
        for value in range(start, stop + 1, field.value.step):
            yield value
    elif field.value.type == values.ValueType.LIST:
        for value in field.value.values:
            yield from generate_values(field.__class__(value.raw))
