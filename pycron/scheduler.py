# Standard libraries
from datetime import datetime

# Third-party libraries
import dateutil.relativedelta as dateutil
import loguru

# Local libraries
import pycron.exception as exceptions
import pycron.generator as generators


class Scheduler(object):
    """Scheduler class used to generate next coming datetimes from a cron rule and a start datetime.

    Args:
        cron (Cron): Each generated datetime must match this cron rule.
        start_datetime (datetime): Datetime from which to start generating datetimes.
        end_datetime (datetime, optional): Generated datetimes must be earlier than end date. Defaults to datetime.max.

    Raises:
        TypeError: When either ``start_datetime``is not a datetime instance.
        TypeError: When either ``end_datetime``is not a datetime instance.
        ValueError: When ``start_datetime`` is earlier than ``end_datetime``.

    """

    def __init__(self, cron, start_datetime, end_datetime=datetime.max):
        # Sanity checks
        if not isinstance(start_datetime, datetime):
            raise TypeError('start_datetime argument must be a datetime instance.')
        if not isinstance(end_datetime, datetime):
            raise TypeError('end_datetime argument must be a datetime instance.')
        if start_datetime >= end_datetime:
            raise ValueError('start_datetime cannot be earlier than end_datetime.')

        # Set attributes
        self._cron = cron
        self._start_datetime = start_datetime
        self._end_datetime = end_datetime

    @property
    def cron(self):
        """datetime: Returns cron instance."""
        return self._cron

    @property
    def start_datetime(self):
        """datetime: Returns start datetime instance."""
        return self._start_datetime

    @property
    def end_datetime(self):
        """datetime: Returns end datetime instance."""
        return self._end_datetime

    def generate_next_datetime(self):
        """Generate next datetime given a cron rule and a start datetime.

        Raises:
            exceptions.NextDateException: When computed datetime is earlier than expected end datetime

        Returns:
            datetime: Next datetime matching the cron rule

        """
        # Generate possible values
        possible_months = sorted(set(month for month in generators.generate_values(self.cron.month)))
        possible_days = sorted(set(day for day in generators.generate_values(self.cron.day_of_month)))
        possible_hours = sorted(set(hour for hour in generators.generate_values(self.cron.hour)))
        possible_minutes = sorted(set(minute for minute in generators.generate_values(self.cron.minutes)))

        # Compute deltas between possible values and current month, day, hour and minute
        months_deltas = list(map(lambda month: month - self.start_datetime.month, possible_months))
        days_deltas = list(map(lambda day: day - self.start_datetime.day, possible_days))
        hours_deltas = list(map(lambda hour: hour - self.start_datetime.hour, possible_hours))
        minutes_deltas = list(map(lambda minute: minute - self.start_datetime.minute, possible_minutes))

        # Reduce the set of possible delta values with the minimum value, and at most the 2 smallest positives values
        possible_month_delta_values = self.min_and_smallest_positives(months_deltas)
        possible_day_delta_values = self.min_and_smallest_positives(days_deltas)
        possible_hour_delta_values = self.min_and_smallest_positives(hours_deltas)
        possible_minute_delta_values = self.min_and_smallest_positives(minutes_deltas)

        # Find the next comming date matching the cron rule
        next_datetime = None
        for month_delta in possible_month_delta_values:
            year_delta = 1 if month_delta < 0 else 0
            for day_delta in possible_day_delta_values:
                for hour_delta in possible_hour_delta_values:
                    for minute_delta in possible_minute_delta_values:
                        # Build delta
                        delta = dateutil.relativedelta(years=year_delta, months=month_delta, days=day_delta,
                                                       hours=hour_delta, minutes=minute_delta)
                        # Compute potential next date
                        possible_date = self.start_datetime + delta
                        # Update next date if date is closer than the previous one
                        if possible_date > self.start_datetime \
                                and (next_datetime is None or possible_date < next_datetime):
                            next_datetime = possible_date

        iterations = len(possible_month_delta_values) * len(possible_day_delta_values) * \
            len(possible_hour_delta_values) * len(possible_minute_delta_values)
        loguru.logger.debug('Possible deltas: {}'.format(iterations))

        # Can't generate a next datetime which is posterior to the end datetime
        if self.end_datetime is not None and next_datetime > self.end_datetime:
            raise exceptions.NextDateException('The next date is posterior to the provided end date.')

        return next_datetime

    @staticmethod
    def min_and_smallest_positives(numbers_list):
        """Extract three useful values from a list (the minimum, zero if in list, and the smallest strictly positive).

        Args:
            numbers_list (list): List of numbers - integers or floats.

        Raises:
            ValueError: When the provided list is empty

        Returns:
            list: three useful integers

        """
        if not numbers_list:
            raise ValueError('numbers_list should not be empty.')
        # Remove duplicates using a set and sort values
        sorted_list = sorted(set(numbers_list))
        # Extract minimum value, 0 if in list, and smallest positive (strictly) values
        expected_values = {sorted_list[0]}
        for value in sorted_list:
            if value >= 0:
                expected_values.add(value)
            if value > 0:
                break
        return sorted(expected_values)
