# Standard libraries
import abc
import collections.abc
import enum
import re

# Local libraries
import pycron.constants as constants
import pycron.exception as exceptions


# Regular expressions
ALWAYS_REGEX = re.compile(r'^\*$')
UNITY_REGEX = re.compile(r'^(\d{1,2}|[a-zA-Z]{3})$')
RANGE_VALUES_REGEX = re.compile(r'(^\d{1,2})-(\d{1,2})$')
STEP_VALUES_REGEX = re.compile(r'^(\*|\d{1,2}-\d{1,2})\/(\d{1,2})$')
LIST_VALUES_REGEX = re.compile(r'^(\*|\d{1,2}|\d{1,2}-\d{1,2}|\d{1,2}-\d{1,2}\/\d{1,2})'
                               r'(,(\*|\d{1,2}|\d{1,2}-\d{1,2}|\d{1,2}-\d{1,2}\/\d{1,2}))+')


class ValueType(enum.Enum):
    """Available types for cron fields."""
    ALWAYS = enum.auto()
    UNITY = enum.auto()
    RANGE = enum.auto()
    STEP = enum.auto()
    LIST = enum.auto()


class ValueFactory(object):
    """Factory class that takes a cron value and build an instance of the appropriate value."""

    @staticmethod
    def create(value):
        """Create an instance of the value, if matching ones.

        Args:
            value (str): Raw cron value as a string

        Raises:
            ValueError: When no user-defined value matches the given input

        Returns:
            ValueBase: An instance of a matching value

        """
        if ALWAYS_REGEX.match(str(value)):
            return Always(str(value))
        elif UNITY_REGEX.match(str(value)):
            return Unity(str(value))
        elif RANGE_VALUES_REGEX.match(str(value)):
            return Range(str(value))
        elif STEP_VALUES_REGEX.match(str(value)):
            return Step(str(value))
        elif LIST_VALUES_REGEX.match(str(value)):
            return List(str(value))
        else:
            raise ValueError('The provided value {} does not match any allowed format.'.format(value))


class ValueBase(abc.ABC):
    """Abstract base class that defines the minimun interface a value must implement.

    Args:
        value (str): Raw cron value as a string

    """

    def __init__(self, value, type=ValueType.ALWAYS):
        try:
            self._value = int(value)
        except ValueError:
            self._value = value
        # Sanity checks on type
        if type not in set(item for item in ValueType):
            raise ValueError('The type of a value must be a valid type defined in ValueType enum.')
        self._type = type

    @abc.abstractmethod
    def __repr__(self):
        """String representation of the class."""
        raise NotImplementedError

    @abc.abstractmethod
    def validate(self, min=None, max=None, extra_values=None):
        """Extra-method used to validate the matching value with optionally a range and some extra values.

        Args:
            min (int, optional): Lower-limit to match. Defaults to None.
            max (int, optional): Upper-limit to match.. Defaults to None.
            extra_values (Iterable, optional): Iterable of extra values that are allowed. Defaults to None.

        """
        raise NotImplementedError

    @property
    def type(self):
        """Returns the type of the value."""
        return self._type

    @property
    @abc.abstractmethod
    def raw(self):
        """Returns the raw of the value, meaning the cron value as a string."""
        raise NotImplementedError

    @property
    def value(self):
        """Returns the value itself."""
        return self._value


class Always(ValueBase):
    """First allowed value: always, meaning that it matches any value of the corresponding field.

    Args:
        value (str): Raw cron value as a string

    Raises:
        ValueError: When the input differs from ``*``.

    """

    def __init__(self, value):
        # Set attributes
        if not ALWAYS_REGEX.match(str(value)):
            raise ValueError('Always value only accept * inputs.')
        super().__init__(value, type=ValueType.ALWAYS)

    def __repr__(self):
        """String representation of the class."""
        return 'Always'

    def validate(self, min=None, max=None, extra_values=None):
        """Extra-method used to validate the matching value with optionally a range and some extra values.

        Args:
            min (int, optional): Lower-limit to match. Defaults to None.
            max (int, optional): Upper-limit to match.. Defaults to None.
            extra_values (Iterable, optional): List/Tuple of extra values that are allowed. Defaults to None.

        """
        return None

    @property
    def raw(self):
        """Returns the raw of the value, meaning the cron value as a string."""
        return '{operator}'.format(operator=constants.ASTERISK_OPERATOR)


class Unity(ValueBase):
    """Second allowed value: unity, meaning that it matches this exact value of the corresponding field.

    Args:
        value (str): Raw cron value as a string

    Raises:
        ValueError: When the input differs from ``*``.

    """

    def __init__(self, value):
        # Set attributes
        if not UNITY_REGEX.match(str(value)):
            raise ValueError('Unity value only accept numbers (< 100) and 3-length strings (JAN, FEB, etc.).')
        super().__init__(value, type=ValueType.UNITY)

    def __repr__(self):
        """String representation of the class."""
        return 'Unity ({})'.format(self.value)

    def validate(self, min=None, max=None, extra_values=None):
        """Extra-method used to validate the value with optionally a range and some extra values.

        Args:
            min (int, optional): Lower-limit to match. Defaults to None.
            max (int, optional): Upper-limit to match.. Defaults to None.
            extra_values (Iterable, optional): List/Tuple of extra values that are allowed. Defaults to None.

        """
        if isinstance(self.value, int):
            if min is not None and min > self.value:
                raise exceptions.ValidationException(f'{self.value} should be greater than {min}.')
            if max is not None and max < self.value:
                raise exceptions.ValidationException(f'{self.value} should be lower than {max}.')
        else:
            if extra_values is not None and self.value.upper() not in set(extra_values):
                raise exceptions.ValidationException(f'{self.value} must intersects the set {extra_values}.')

    @property
    def raw(self):
        """Returns the raw of the value, meaning the cron value as a string."""
        return str(self._value)


class Range(ValueBase):

    def __init__(self, value):
        # Set attributes
        if not RANGE_VALUES_REGEX.match(str(value)):
            raise ValueError('Range value only accepts numbers (< 100) separated with a hyphen (i.e. 4-5 or 20-45).')
        super().__init__(value, type=ValueType.RANGE)
        # Extract min and max values of range
        result = RANGE_VALUES_REGEX.search(value)
        range_min = Unity(result.group(1))
        range_max = Unity(result.group(2))
        if int(range_min.value) > int(range_max.value):
            raise ValueError('First value of a range must be lower than second value (not strictly)')
        self._range_min, self._range_max = range_min, range_max

    def __repr__(self):
        """String representation of the class."""
        return 'Range (min={} - max={})'.format(self.min, self.max)

    def validate(self, min=None, max=None, extra_values=None):
        """Extra-method used to validate the matching value with optionally a range and some extra values.

        Args:
            min (int, optional): Lower-limit to match. Defaults to None.
            max (int, optional): Upper-limit to match.. Defaults to None.
            extra_values (Iterable, optional): List/Tuple of extra values that are allowed. Defaults to None.

        """
        # TODO: ranges seems to be standard only with digits
        self.min_value.validate(min=min, max=max, extra_values=extra_values)
        self.max_value.validate(min=min, max=max, extra_values=extra_values)

    @property
    def raw(self):
        """Returns the raw of the value, meaning the cron value as a string."""
        return '{min}{operator}{max}'.format(min=self.min, operator=constants.HYPHEN_OPERATOR, max=self.max)

    @property
    def min(self):
        """Returns the lower limit of the range as an integer."""
        return self._range_min.value

    @property
    def min_value(self):
        """Returns the lower limit of the range as a Unity."""
        return self._range_min

    @property
    def max(self):
        """Returns the upper limit of the range as an integer."""
        return self._range_max.value

    @property
    def max_value(self):
        """Returns the upper limit of the range as a Unity."""
        return self._range_max


class Step(ValueBase):

    def __init__(self, value):
        # Set attributes
        if not STEP_VALUES_REGEX.match(value):
            raise ValueError('Step value only accept * and ranges (i.e. */5 and 10-30/6)')
        super().__init__(value, type=ValueType.STEP)
        # Extract value or range and step values
        result = STEP_VALUES_REGEX.search(value)
        self._step = int(result.group(2))
        self._value = ValueFactory.create(result.group(1))

    def __repr__(self):
        """String representation of the class."""
        return 'Step (value={} - step={})'.format(self.value, self.step)

    def validate(self, min=None, max=None, extra_values=None):
        # Validate either Always or Range (cascading validation)
        self.value.validate(min=min, max=max, extra_values=extra_values)

    @property
    def raw(self):
        """Returns the raw of the value, meaning the cron value as a string."""
        return '{value}{operator}{step}'.format(value=self.value.raw, operator=constants.SLASH_OPERATOR, step=self.step)

    @property
    def step(self):
        return self._step


class List(ValueBase, collections.abc.Sequence):

    def __init__(self, value):
        # Set attributes
        if not LIST_VALUES_REGEX.match(value):
            raise ValueError('A list value is only made with always, unity, step and ranges, separated with a comma.')
        super().__init__(value, type=ValueType.LIST)
        # Try to extract values from list
        self._values = [ValueFactory.create(val) for val in value.split(constants.COMMA_OPERATOR)]

    def __repr__(self):
        """String representation of the class."""
        return 'List ({})'.format(self.values)

    def __getitem__(self, index):
        return self._values[index]

    def __len__(self):
        return len(self._values)

    def validate(self, min=None, max=None, extra_values=None):
        # Validate either Always or Range (cascading validation)
        # TODO: iterable?
        for value in self.values:
            value.validate(min=min, max=max, extra_values=extra_values)

    @property
    def values(self):
        return self._values

    @property
    def raw(self):
        """Returns the raw of the value, meaning the cron value as a string."""
        return '{operator}'.format(operator=constants.COMMA_OPERATOR).join([value.raw for value in self.values])
