# Standard libraries
import io
import re
import setuptools


# Define packages that are required to use that project
REQUIRED_PACKAGES = ['loguru', 'python-dateutil']

# Some extra packages for linting, testing and documentation building
LINT_PACKAGES = [
    'flake8', 'flake8-bandit', 'flake8-bugbear', 'flake8-comprehensions', 'flake8-docstrings',
    'flake8-eradicate', 'flake8-mutable'
]
TEST_PACKAGES = ['pytest', 'pytest-cov']
DOCS_PACKAGES = ['sphinx', 'sphinx-rtd-theme']

# Extract version
with io.open('pycron/__init__.py', 'rt', encoding='utf8') as f:
    version = re.search(r'__version__ = \'(.*?)\'', f.read(), re.M).group(1)

# Extract description
with open('README.md', 'r') as fh:
    long_description = fh.read()

# Setup
setuptools.setup(
    name='pycron',
    version=version,
    author='Alexandre MAYEROWITZ',
    author_email='alexandre.mayerowitz@gmail.com',
    description='PyCron toolset',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/Red-Sky/pycron',
    packages=setuptools.find_packages(),
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Framework :: Flake8',
        'Framework :: Pytest',
        'Framework :: Sphinx',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        # TODO: add topics?
    ],
    python_requires='>=3.5',
    install_requires=REQUIRED_PACKAGES,
    extras_require={
        'lint': LINT_PACKAGES,
        'tests': TEST_PACKAGES,
        'docs': DOCS_PACKAGES
    })
