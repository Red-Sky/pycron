# Standard libraries
import pycron.cron as crons
import pycron.field as fields
import pycron.value as values


class TestCron(object):

    def test_constructor(self):
        value = '*,1,*/3,5-6,3-4/5'
        cron = crons.Cron(minutes=value, hour=value, day_of_month=value, month=value, day_of_week=value)

        assert isinstance(cron._minutes, fields.Minutes)
        assert isinstance(cron._hour, fields.Hour)
        assert isinstance(cron._day_of_month, fields.DayOfMonth)
        assert isinstance(cron._month, fields.Month)
        assert isinstance(cron._day_of_week, fields.DayOfWeek)
        print(crons.Cron())

    def test_minutes_setter(self):
        # Init with step value
        cron = crons.Cron(minutes='*/10')
        assert isinstance(cron.minutes, fields.Minutes)
        assert isinstance(cron.minutes.value, values.Step)
        assert isinstance(cron.minutes.value.value, values.Always)
        assert cron.minutes.value.step == 10
        # Update minutes with range
        cron.minutes = '5-10'
        assert isinstance(cron.minutes, fields.Minutes)
        assert isinstance(cron.minutes.value, values.Range)
        assert cron.minutes.value.min == 5
        assert cron.minutes.value.max == 10

    def test_hour_setter(self):
        # Init with step value
        cron = crons.Cron(hour='*/10')
        assert isinstance(cron.hour, fields.Hour)
        assert isinstance(cron.hour.value, values.Step)
        assert isinstance(cron.hour.value.value, values.Always)
        assert cron.hour.value.step == 10
        # Update with range
        cron.hour = '5-10'
        assert isinstance(cron.hour, fields.Hour)
        assert isinstance(cron.hour.value, values.Range)
        assert cron.hour.value.min == 5
        assert cron.hour.value.max == 10

    def test_day_of_month_setter(self):
        # Init with step value
        cron = crons.Cron(day_of_month='*/10')
        assert isinstance(cron.day_of_month, fields.DayOfMonth)
        assert isinstance(cron.day_of_month.value, values.Step)
        assert isinstance(cron.day_of_month.value.value, values.Always)
        assert cron.day_of_month.value.step == 10
        # Update with range
        cron.day_of_month = '5-10'
        assert isinstance(cron.day_of_month, fields.DayOfMonth)
        assert isinstance(cron.day_of_month.value, values.Range)
        assert cron.day_of_month.value.min == 5
        assert cron.day_of_month.value.max == 10

    def test_month_setter(self):
        # Init with step value
        cron = crons.Cron(month='*/10')
        assert isinstance(cron.month, fields.Month)
        assert isinstance(cron.month.value, values.Step)
        assert isinstance(cron.month.value.value, values.Always)
        assert cron.month.value.step == 10
        # Update with range
        cron.month = '5-10'
        assert isinstance(cron.month, fields.Month)
        assert isinstance(cron.month.value, values.Range)
        assert cron.month.value.min == 5
        assert cron.month.value.max == 10

    def test_day_of_week_setter(self):
        # Init with step value
        cron = crons.Cron(day_of_week='*/10')
        assert isinstance(cron.day_of_week, fields.DayOfWeek)
        assert isinstance(cron.day_of_week.value, values.Step)
        assert isinstance(cron.day_of_week.value.value, values.Always)
        assert cron.day_of_week.value.step == 10
        # Update with range
        cron.day_of_week = '1-5'
        assert isinstance(cron.day_of_week, fields.DayOfWeek)
        assert isinstance(cron.day_of_week.value, values.Range)
        assert cron.day_of_week.value.min == 1
        assert cron.day_of_week.value.max == 5
