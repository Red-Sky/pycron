# Third-party libraries
import pytest

# Standard libraries
import pycron.constants as constants
import pycron.exception as exceptions
import pycron.field as fields
import pycron.value as values


class TestField(object):

    def test_constructor(self):
        # Always
        field = fields.Field('*')
        assert isinstance(field._value, values.Always)
        # Unity
        field = fields.Field('5')
        assert isinstance(field._value, values.Unity)
        field = fields.Field('jan')
        assert isinstance(field._value, values.Unity)
        # Range
        field = fields.Field('5-10')
        assert isinstance(field._value, values.Range)
        # Step
        field = fields.Field('*/5')
        assert isinstance(field._value, values.Step)
        # List
        field = fields.Field('1,*,*/2,4-5')
        assert isinstance(field._value, values.List)
        # Invalid
        with pytest.raises(exceptions.ValidationException):
            fields.Field('61', max=59)
        with pytest.raises(exceptions.ValidationException):
            fields.Field('61', min=70)
        with pytest.raises(exceptions.ValidationException):
            fields.Field('60', max=59, extra_values=(60, ))
        with pytest.raises(ValueError):
            fields.Field('5-10', type='5')
        fields.Field('60,61', extra_values=(60, 61, 62))
        fields.Field('60,61,62,63,64', extra_values=(60, 61, 62, 63))
        fields.Field('60,61,62,63', extra_values=(60, 61, 62, 63))

    def test__repr__(self):
        assert isinstance(fields.Field('*').__repr__(), str)
        assert len(fields.Field('*').__repr__())

    def test_min(self):
        assert fields.Field('*').min is None
        assert fields.Field('1,*,*/2,4-5', min=0).min == 0
        assert fields.Field('1,*,*/2,4-5', min=1).min == 1

    def test_max(self):
        assert fields.Field('*').max is None
        assert fields.Field('1,*,*/2,4-5', max=20).max == 20
        assert fields.Field('1,*,*/2,4-5', max=25).max == 25

    def test_type(self):
        assert fields.Field('*').type == fields.FieldType.MINUTES
        assert fields.Field('1,*,*/2,4-5', type=fields.FieldType.MONTH).type == fields.FieldType.MONTH


class TestMinutes(object):

    def test_constructor(self):
        with pytest.raises(exceptions.ValidationException):
            fields.Minutes('60')
        fields.Minutes('59')


class TestHour(object):

    def test_constructor(self):
        with pytest.raises(exceptions.ValidationException):
            fields.Hour('24')
        fields.Hour('23')


class TestDayOfMonth(object):

    def test_constructor(self):
        with pytest.raises(exceptions.ValidationException):
            fields.DayOfMonth('0')
        with pytest.raises(exceptions.ValidationException):
            fields.DayOfMonth('32')
        fields.DayOfMonth('31')


class TestMonth(object):

    def test_constructor(self):
        with pytest.raises(exceptions.ValidationException):
            fields.Month('13')
        with pytest.raises(exceptions.ValidationException):
            fields.Month('0')
        for index in range(1, 13):
            fields.Month(index)
        for extra_value in constants.MONTH_EXTRA_VALUES:
            fields.Month(extra_value)


class TestDayOfWeek(object):

    def test_constructor(self):
        with pytest.raises(exceptions.ValidationException):
            fields.DayOfWeek('7')
        fields.DayOfWeek('6')
        with pytest.raises(exceptions.ValidationException):
            fields.DayOfWeek('AAA')
        for extra_value in constants.DAY_OF_WEEK_EXTRA_VALUES:
            fields.DayOfWeek(extra_value)
