# Third-party libraries
import pytest

# Local libraries
import pycron.field as fields
import pycron.generator as generators


def test_generate_values():
    # Sanity chacks
    with pytest.raises(ValueError):
        for _ in generators.generate_values('3435435'):
            pass

    # Generate always
    expected_values = [value for value in range(60)]
    for index, value in enumerate(generators.generate_values(fields.Minutes('*'))):
        assert value == expected_values[index]

    # Generate unity
    expected_values = [5]
    for index, value in enumerate(generators.generate_values(fields.Minutes('5'))):
        assert value == expected_values[index]
    expected_values = [4]
    for index, value in enumerate(generators.generate_values(fields.Month('apr'))):
        assert value == expected_values[index]
    expected_values = [3]
    for index, value in enumerate(generators.generate_values(fields.DayOfWeek('wed'))):
        assert value == expected_values[index]

    # Generate range
    expected_values = [value for value in range(21, 34)]
    for index, value in enumerate(generators.generate_values(fields.Minutes('21-33'))):
        assert value == expected_values[index]

    # Generate step
    expected_values = [value for value in range(0, 60, 3)]
    for index, value in enumerate(generators.generate_values(fields.Minutes('*/3'))):
        assert value == expected_values[index]
    expected_values = [value for value in range(14, 31, 3)]
    for index, value in enumerate(generators.generate_values(fields.Minutes('14-31/3'))):
        assert value == expected_values[index]