# Standard libraries
import datetime

# Third-party libraries
import pytest

# Local libraries
import pycron.cron as crons
import pycron.exception as exceptions
import pycron.scheduler as schedulers


def test_min_and_smallest_values():
    # Sanity checks
    with pytest.raises(ValueError):
        schedulers.Scheduler.min_and_smallest_positives([])
    # Tests
    # 3 cas de figure
    # [-5, 0, 2] -> [-5, 0, 2]
    numbers_list = [2, -5, 0]
    result = schedulers.Scheduler.min_and_smallest_positives(numbers_list)
    assert result == [-5, 0, 2]

    # [-5, 0] -> [-5, 0]
    numbers_list = [-5, 0]
    result = schedulers.Scheduler.min_and_smallest_positives(numbers_list)
    assert result == [-5, 0]

    # [-5, 1, 2] -> [-5, 1]
    numbers_list = [2, -5, 1]
    result = schedulers.Scheduler.min_and_smallest_positives(numbers_list)
    assert result == [-5, 1]

    # [0] -> [0]
    numbers_list = [0]
    result = schedulers.Scheduler.min_and_smallest_positives(numbers_list)
    assert result == [0]

    # [1] -> [1]
    numbers_list = [1]
    result = schedulers.Scheduler.min_and_smallest_positives(numbers_list)
    assert result == [1]

    # [1, 2] -> [1]
    numbers_list = [1, 2]
    result = schedulers.Scheduler.min_and_smallest_positives(numbers_list)
    assert result == [1]


class TestGenerateNextDate(object):

    def test_constructor(self):
        # Parameters
        cron = crons.Cron(minutes='15,10,25', hour='14,16,18', day_of_month='11', month='1,5', day_of_week='*')

        # Sanity checks
        with pytest.raises(TypeError):
            schedulers.Scheduler(cron, None)
        with pytest.raises(TypeError):
            schedulers.Scheduler(cron, datetime.datetime.min, end_datetime=None)
        with pytest.raises(ValueError):
            schedulers.Scheduler(cron, datetime.datetime.max, end_datetime=datetime.datetime.max)

        # Instancitation that works
        start_datetime = datetime.datetime(2020, 2, 1, 15, 0, 0)
        end_datetime = datetime.datetime(2020, 2, 1, 15, 0, 1)
        schedulers.Scheduler(cron, start_datetime, end_datetime)

    def test_start_datetime(self):
        cron = crons.Cron(minutes='15,10,25', hour='14,16,18', day_of_month='11', month='1,5', day_of_week='*')
        start_datetime = datetime.datetime(2020, 2, 1, 15, 0, 0)
        scheduler = schedulers.Scheduler(cron, start_datetime)

        assert scheduler.start_datetime == start_datetime

    def test_end_datetime(self):
        cron = crons.Cron(minutes='15,10,25', hour='14,16,18', day_of_month='11', month='1,5', day_of_week='*')
        start_datetime = datetime.datetime(2020, 2, 1, 15, 0, 0)
        end_datetime = datetime.datetime(2020, 2, 1, 16, 0, 0)
        scheduler = schedulers.Scheduler(cron, start_datetime, end_datetime)

        assert scheduler.end_datetime == end_datetime

    def test_generate_next_date(self):

        # Define cron to use
        cron = crons.Cron(minutes='15,10,25', hour='14,16,18', day_of_month='11', month='1,5', day_of_week='*')

        # Dates scenario
        scenario = [
            # Minutes
            ('11-05-2020 14:09', '11-05-2020 14:10'),
            ('11-05-2020 14:10', '11-05-2020 14:15'),
            ('11-05-2020 14:24', '11-05-2020 14:25'),
            ('11-05-2020 14:25', '11-05-2020 16:10'),
            ('11-05-2020 14:30', '11-05-2020 16:10'),
            ('11-05-2020 18:25', '11-01-2021 14:10'),
            # Hours
            ('11-05-2020 13:11', '11-05-2020 14:10'),
            ('11-05-2020 14:11', '11-05-2020 14:15'),
            ('11-05-2020 15:11', '11-05-2020 16:10'),
            ('11-05-2020 16:11', '11-05-2020 16:15'),
            ('11-05-2020 18:11', '11-05-2020 18:15'),
            ('11-05-2020 18:25', '11-01-2021 14:10'),
            ('11-05-2020 19:11', '11-01-2021 14:10'),
            # Days
            ('10-05-2020 18:11', '11-05-2020 14:10'),
            ('11-05-2020 18:11', '11-05-2020 18:15'),
            ('12-05-2020 18:11', '11-01-2021 14:10'),
            # Months
            ('11-04-2020 18:11', '11-05-2020 14:10'),
            ('11-05-2020 18:11', '11-05-2020 18:15'),
            ('11-06-2020 18:11', '11-01-2021 14:10')
        ]

        # Run tests
        for start_date_str, expected_date_str in scenario:
            # Parse dates
            start_datetime = datetime.datetime.strptime(start_date_str, '%d-%m-%Y %H:%M')
            expected_datetime = datetime.datetime.strptime(expected_date_str, '%d-%m-%Y %H:%M')
            scheduler = schedulers.Scheduler(cron, start_datetime)
            next_date = scheduler.generate_next_datetime()
            assert next_date.timestamp() == expected_datetime.timestamp()

        # Error when next date is greater than given end date
        with pytest.raises(exceptions.NextDateException):
            cron = crons.Cron(minutes='15,10,25', hour='14,16,18', day_of_month='11', month='1,5', day_of_week='*')
            start_datetime = datetime.datetime(2020, 2, 1, 15, 0, 0)
            end_datetime = datetime.datetime(2020, 2, 1, 15, 0, 1)
            scheduler = schedulers.Scheduler(cron, start_datetime, end_datetime=end_datetime)
            scheduler.generate_next_datetime()
