# Third-party libraries
import pytest

# Standard libraries
import pycron.value as values


class TestValueFactory(object):

    def test_create(self):
        # Always
        value = values.ValueFactory.create('*')
        assert isinstance(value, values.Always)
        # Unity
        value = values.ValueFactory.create('5')
        assert isinstance(value, values.Unity)
        value = values.ValueFactory.create('jan')
        assert isinstance(value, values.Unity)
        # Range
        value = values.ValueFactory.create('4-5')
        assert isinstance(value, values.Range)
        # Step
        value = values.ValueFactory.create('*/5')
        assert isinstance(value, values.Step)
        # List
        value = values.ValueFactory.create('5,6,7,8,9')
        assert isinstance(value, values.List)
        # Invalid
        with pytest.raises(ValueError):
            values.ValueFactory.create('blabla')


class TestValueBase(object):

    def test_constructor(self):
        # Trick: set abstract methods to an empty set to instantiate abtract classes
        values.ValueBase.__abstractmethods__ = set()
        # Tests
        value = values.ValueBase('1')
        assert isinstance(value._value, int)
        value = values.ValueBase('test')
        assert isinstance(value._value, str)
        # Invalud type
        with pytest.raises(ValueError):
            values.ValueBase('1', type='test')

    def test__repr__(self):
        with pytest.raises(NotImplementedError):
            values.ValueBase('*').__repr__()

    def test_validate(self):
        with pytest.raises(NotImplementedError):
            values.ValueBase('*').validate()

    def test_raw(self):
        with pytest.raises(NotImplementedError):
            values.ValueBase('*').raw

    def test_type(self):
        assert values.ValueBase('*', type=values.ValueType.ALWAYS).type == values.ValueType.ALWAYS
        assert values.ValueBase('*', type=values.ValueType.UNITY).type == values.ValueType.UNITY

    def test_value(self):
        assert values.ValueBase('*').value == '*'
        assert values.ValueBase('5').value == 5
        assert values.ValueBase('test_123').value == 'test_123'


class TestAlways(object):

    def test_constructor(self):
        # Invalid values
        with pytest.raises(ValueError):
            values.Always('5')
        with pytest.raises(ValueError):
            values.Always('jan')
        with pytest.raises(ValueError):
            values.Always('5-5')
        with pytest.raises(ValueError):
            values.Always('*/5')
        with pytest.raises(ValueError):
            values.Always('5,5,6')
        # Valid values
        value = values.Always('*')
        assert value._value == '*'

    def test__repr__(self):
        assert isinstance(values.Always('*').__repr__(), str)
        assert len(values.Always('*').__repr__())

    def test_type(self):
        assert values.Always('*').type == values.ValueType.ALWAYS

    def test_raw(self):
        assert values.Always('*').raw == '*'


class TestUnity(object):

    def test_constructor(self):
        # Invalid values
        with pytest.raises(ValueError):
            values.Unity('*')
        with pytest.raises(ValueError):
            values.Unity('5-5')
        with pytest.raises(ValueError):
            values.Unity('*/5')
        with pytest.raises(ValueError):
            values.Unity('5,5,6')
        # Valid values
        value = values.Unity('5')
        assert value._value == 5
        value = values.Unity('jan')
        assert value._value == 'jan'

    def test__repr__(self):
        assert isinstance(values.Unity('5').__repr__(), str)
        assert len(values.Unity('5').__repr__())

    def test_type(self):
        assert values.Unity('5').type == values.ValueType.UNITY
        assert values.Unity('jan').type == values.ValueType.UNITY

    def test_raw(self):
        assert values.Unity('5').raw == '5'


class TestRange(object):

    def test_constructor(self):
        # Invalid values
        with pytest.raises(ValueError):
            values.Range('*')
        with pytest.raises(ValueError):
            values.Range('5')
        with pytest.raises(ValueError):
            values.Range('jan')
        with pytest.raises(ValueError):
            values.Range('*/5')
        with pytest.raises(ValueError):
            values.Range('5,5,6')
        with pytest.raises(ValueError):
            values.Range('5-4')
        # Valid values
        value = values.Range('5-10')
        assert value._value == '5-10'
        assert isinstance(value._range_min, values.Unity)
        assert isinstance(value._range_max, values.Unity)
        assert value._range_min.value == 5
        assert value._range_max.value == 10

    def test__repr__(self):
        assert isinstance(values.Range('5-10').__repr__(), str)
        assert len(values.Range('5-10').__repr__())

    def test_type(self):
        assert values.Range('5-10').type == values.ValueType.RANGE

    def test_min(self):
        assert values.Range('5-10').min == 5

    def test_min_value(self):
        assert isinstance(values.Range('5-10').min_value, values.Unity)
        assert values.Range('5-10').min_value.value == 5

    def test_max(self):
        assert values.Range('5-10').max == 10

    def test_max_value(self):
        assert isinstance(values.Range('5-10').max_value, values.Unity)
        assert values.Range('5-10').max_value.value == 10

    def test_raw(self):
        assert values.Range('5-10').raw == '5-10'


class TestStep(object):

    def test_constructor(self):
        # Invalid values
        with pytest.raises(ValueError):
            values.Step('*')
        with pytest.raises(ValueError):
            values.Step('5')
        with pytest.raises(ValueError):
            values.Range('jan')
        with pytest.raises(ValueError):
            values.Step('5-5')
        with pytest.raises(ValueError):
            values.Step('5,5,6')
        # Valid values
        value = values.Step('*/10')
        assert isinstance(value._value, values.Always)
        assert value._value._value == '*'
        assert value._step == 10

        value = values.Step('5-10/10')
        assert isinstance(value._value, values.Range)
        assert value._value._value == '5-10'
        assert value._value.min == 5
        assert value._value.max == 10

    def test__repr__(self):
        assert isinstance(values.Step('*/10').__repr__(), str)
        assert len(values.Step('*/10').__repr__())

    def test_type(self):
        assert values.Step('*/10').type == values.ValueType.STEP

    def test_step(self):
        assert values.Step('*/10').step == 10

    def test_raw(self):
        assert values.Step('*/10').raw == '*/10'
        assert values.Step('5-10/3').raw == '5-10/3'


class TestList(object):

    def test_constructor(self):
        # Invalid values
        with pytest.raises(ValueError):
            values.List('*')
        with pytest.raises(ValueError):
            values.List('5')
        with pytest.raises(ValueError):
            values.Range('jan')
        with pytest.raises(ValueError):
            values.List('5-5')
        with pytest.raises(ValueError):
            values.List('*/10')
        # Valid values
        value = values.List('*,1,2-3,*/10')
        assert value._value == '*,1,2-3,*/10'

        assert isinstance(value[0], values.Always)
        assert isinstance(value[1], values.Unity)
        assert isinstance(value[2], values.Range)
        assert isinstance(value[3], values.Step)
        assert len(value) == 4

    def test__repr__(self):
        assert isinstance(values.List('*,1,2-3,*/10').__repr__(), str)
        assert len(values.List('*,1,2-3,*/10').__repr__())

    def test_type(self):
        assert values.List('*,1,2-3,*/10').type == values.ValueType.LIST

    def test_raw(self):
        assert values.List('*,1,2-3,*/10').raw == '*,1,2-3,*/10'
        assert values.List('*,1').raw == '*,1'
